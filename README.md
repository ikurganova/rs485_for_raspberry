# rs485_for_raspberry


Этот проект - пример реализации интерфейса rs485 для raspberry Pi 3.  
В проекте использовано многопоточное программирование для обеспечения бесперебойного и надежного обмена данными независимо от их обработки и основоного алгоритма программы. 
В проекте используются библиотеки boost (для работы с последовательным портом) и bcm2835 (для управления ногой переключения приёма/передачи).



This project is an example of the implementation of the rs485 interface for raspberry Pi 3. 
The project uses multithreaded programming to ensure  reliable data exchange regardless of basic algorithm of the program. 
The project uses boost (for working with the serial port) and bcm2835 (for controlling the transmit-receive pin).