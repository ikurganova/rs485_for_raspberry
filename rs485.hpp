#include <iostream>
#include <thread>
#include <queue>
#include <bcm2835.h>
#include <chrono>
#include <mutex>


class rs485
{
	std::mutex _lock;
	std::queue <std::string> out;
	unsigned int time1symb;
	void claculate_time1symb(unsigned int baud_rate)
	{
		time1symb = 1000000; // мкс в с
		time1symb /= baud_rate; // время для передачи 1 бита
		time1symb *= 10; // 10 бит требуется для передачи одного байта
		time1symb /= 1000; // перевод в мс
	}
	
	unsigned char pinCtrl;
	std::thread thRead;
	std::thread thWrite;
	
	public:
	rs485(std::string port, unsigned int baud_rate, unsigned char pinCtrl_) 
	: io()
	, serial(io,port)
	, pinCtrl(pinCtrl_)
	, thRead(&rs485::read, this)
	, thWrite(&rs485::ctrl, this)
	{
		serial.set_option(boost::asio::serial_port_base::baud_rate(baud_rate));
		claculate_time1symb(baud_rate);
		
		bcm2835_delay(100);
		bcm2835_init();
		bcm2835_gpio_fsel(pinCtrl, BCM2835_GPIO_FSEL_OUTP);	
		send("12345");
		std::cout<< "rs485"<< std::endl;
	}
	
	~rs485()
	{
		thRead.join();
		thWrite.join();
	}
	
    
    
    void send (std::string msg)
    {
		_lock.lock();
		out.push(msg);
		_lock.unlock();
	}
	
	private:
	
	void read()
	{
		bcm2835_delay(2100);

		for (;;)
		{
		char c;
		boost::asio::read(this->serial,boost::asio::buffer(&c,1));
		std::cout<< c << std::endl;
		}
	}
	
	void write (std::string msg)
	{
		
		bcm2835_gpio_write(pinCtrl, HIGH);
		boost::asio::write(serial,boost::asio::buffer(msg.c_str(),msg.size()));
		delay((time1symb * msg.size()) + 1);
		bcm2835_gpio_write(pinCtrl, LOW);

	}
	
	void ctrl()
	{
		for (;;)
		{
			if (!out.empty())
			{
				write(out.front());
				out.pop();
			}
		}
	}
	
	
	public:
	
    boost::asio::io_service io;
    boost::asio::serial_port serial;
};

